
/**
* Please install gulp and the required modules typing the following comands 
* in your shell terminal, before use this gulp script
* 
* @use npm install gulp - This command installs gulp 
* @use npm install --global gulp-cli - Use only if gulp doesn't work from prompt
* 
* @use npm install --save gulp-install
* @use npm install --save-dev gulp-install
* 
* @use npm install --save-dev gulp-clean 
* @use npm install --save-dev gulp-uglify
* @use npm install --save-usedev gulp-rename
* @use npm install --save-dev gulp-util
* @use npm install --save-dev gulp-jshint
* @use npm install --save-dev gulp-order
* @use npm install --save-dev gulp-concat
* @use npm install --save-dev gulp-ftp
* @use npm install --save-dev gulp-sftp
* @use npm install --save-dev gulp-prompt
* @use npm install --save-dev gulp-minify
* @use npm install --save-dev gulp-strip-comments
* @use npm install --save-dev jshint-stylish
* @use npm install --save-dev gulp-load-plugins
*
* How to launch gulp
* 
* @use gulp --live - To export scripts.js to deploy on Live environment 
* @use gulp --test - To export scripts.js to deploy on Dev environment (not minified nor stripped) 
*/

/**
 * Sets up global variables
 */

var isLive    = false;
var isDev     = true;

var gulp      = require('gulp'),
    clean     = require('gulp-clean'),
    uglify    = require('gulp-uglify'),
    rename    = require('gulp-rename'),
    sass      = require('gulp-sass'),
    gutil     = require('gulp-util'),
    jshint    = require('gulp-jshint'),
    order     = require("gulp-order"),
    concat    = require("gulp-concat"),
    ftp       = require('gulp-ftp'),
    prompt    = require('gulp-prompt'),
    minify    = require('gulp-minify'),
    strip     = require('gulp-strip-comments'),
    stylish   = require('jshint-stylish'),
    plugins   = require('gulp-load-plugins')(),
    cssmin    = require('gulp-cssmin'),
    paths     = {
      tocompress:   'js/partials/_js/_*.js',
      nocompress:   'js/partials/_libs/_*.js',
      scssFolder:   'sass/',
      scssFilter:   '**/*.scss',
      cssFolder:    'css/',
      cssFilter:    '**/*.css',
      checksyntax:  '',
      tmpfolder:    '_tmp/',
      dist:         'js/'  /* ruta a mi directorio de SVN, así no has de hacer un copy+paste*/
    };

/**
 * Sets to true the boolean var to select the enviromment
 * 
 * @return {boolean} The selected environment
 */

function environmentDetect() {

  if (gutil.env.dev === true) {
    gutil.log(gutil.env.dev);
    isLive = false;
    isDev  = false;
    return gutil.env.dev;
  }
  else if (gutil.env.stage === true) {
    gutil.log(gutil.env.stage);
    isLive = false;
    isDev  = true;
    return gutil.env.stage;
  }
  else if (gutil.env.live === true) {
    gutil.log(gutil.env.live);
    isLive = true;
    isDev = false;
    return gutil.env.live;
  }
}

/**
 * Task to check errors on javascript files
 * 
 * @see .jshintrc file at root for the values
 * @return A log reporting error(s)
 */

gulp.task('corregir', function() {
  return gulp.src(paths.checksyntax)
  .pipe(plugins.jshint())
  .pipe(plugins.jshint.reporter(stylish));
});

/**
*  Task to compress files
*  
*  @var {string} paths.tocompress - The url of the files to be compressed
*  @var {string} paths.tmpfolder - The url of the temporally folder
*/

gulp.task('compress', ['corregir'], function() {
  
  environmentDetect();

  if (isDev === true) {
    return gulp.src(paths.tocompress)
      .pipe(gulp.dest(paths.tmpfolder)
      .on('end', function() {
      gutil.log('0 files have been compressed on Dev environment');
    }));
  }
  else if (isLive === true) {
    return gulp.src(paths.tocompress)
      .pipe(uglify()).pipe(strip())
      .pipe(gulp.dest(paths.tmpfolder)
      .on('end', function() {
      gutil.log('All files compressed succesfully'); 
    }));
  }

});

/**
* Task to copying files that don't need to compress
*
* @var {string} paths.nocompress - The source folder
* @var {string} paths.tmpfolder - The temporally destination folder
*/

gulp.task('copy', ['compress'], function() {
  
  if (isDev === true) {
    return gulp.src(paths.nocompress)
      .pipe(gulp.dest(paths.tmpfolder));
  }
  else if (isLive === true) {
    return gulp.src(paths.nocompress)
      .pipe(uglify())
      .pipe(strip())
      .pipe(gulp.dest(paths.tmpfolder));
  }
});

/**
* Task to order files before to be merged and uglifyed
* 
* @var {string} paths.tmpfolder - The temporally source folder
* @var {string} paths.dist - The destination folder
*/

gulp.task('order', ['copy'], function() {
  return gulp.src([
    paths.tmpfolder + "_libraries.js",
    paths.tmpfolder + "_vars.js",
    paths.tmpfolder + "_script.js",
    paths.tmpfolder + "_handlers.js",
    paths.tmpfolder + "_onReady.js"
    ]) 
.pipe(concat('scripts.tmp.js'))
.pipe(gulp.dest(paths.dist));

});

/**
* Task to remove files & folders that are no longer being needed
*
* @var {string} paths.dist - The source folder
*/

gulp.task('clean', ['order'], function() {
  return gulp.src([paths.tmpfolder])
  .pipe(clean({force: true}));
});


gulp.task('sass', ['clean'], function() {
    gulp.src(paths.scssFolder + paths.scssFilter)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(paths.cssFolder))
});

gulp.task('minifyCSS', ['sass'], function() {
    gulp.src(paths.cssFolder + paths.cssFilter)
    .pipe(clean({force: true}))
    .pipe(cssmin())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('css'));
});


/**
* Task to select the enviromment where the scripts will be deployed
* The script only will be uglifyed on Live environment
*
* @var {boolean} [isDev = true]
* @var {boolean} [isLive = true]
* @var {string} paths.dist - The source folder
*/

gulp.task('deploy', ['minifyCSS'], function() {

  if (isDev === true) {
    gulp.src(paths.dist + 'scripts.tmp.js')
    .pipe(clean({force: true}))
    .pipe(rename('scripts.dev.js'))
    .pipe(gulp.dest(paths.dist));order
  }
  else if (isLive === true) {
    gulp.src(paths.dist + 'scripts.tmp.js')
    .pipe(clean({force: true}))
    .pipe(uglify())
    .pipe(rename('scripts.min.js'))
    .pipe(gulp.dest(paths.dist));
  }
});

gulp.task('default', ['compress', 'copy', 'order', 'clean', 'sass', 'minifyCSS', 'deploy'],function(){
      // gulp.watch(paths.scssFolder + paths.scssFilter,['sass']);
});

