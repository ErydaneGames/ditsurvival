/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);
window.ditGames = window.ditGames || {};
/********************************************************************************************************************/
/* @Start 	- Variables globales */
ditGames.vars = {
  
  infoUser: {
                'name':   "Christian",
                'edad':   35,
                'yPosP':   0,
                'xPosP':   0
  },
  scoreLevel:   1,
  level:        1
    
}

/* @end 	- Variables globales */
/********************************************************************************************************************/
/********************************************************************************************************************/
/* @Start 	- Custom Body */

$('body').attr('id', 'dit-wrapper');
$('body').addClass('dit-body');

/* @End  	- Custom Body */
/********************************************************************************************************************/
/**
 * @file dit.js
 *
 *
 */
ditGames.counter = {
    init: function() {
        ditGames.vars.level++;
        $('.level').text(ditGames.vars.level);
    },
    clear: function(){
        clearTimeout();
    }
};

ditGames.dit = {
    init: function() {
        $('#box').draggable({
            containment: "parent"
        });
    },
    touches: function(){
        jQuery('body').on('touchstart touchend touchmove', function(e) {
            var xPos = e.originalEvent.touches[0].pageX;
            var yPos = e.originalEvent.touches[0].pageY;
            ditGames.vars.infoUser.yPosP = yPos;
            ditGames.vars.infoUser.xPosP = xPos;
            $('.posicion').text('Left: ' + xPos + ' /  Top: ' + yPos);
            $('#box').css({
                top: (yPos - 50),
                left: (xPos - 50)
            });
        });
        jQuery('body').on('touchstart touchmove', function(e) {
            ditGames.counter.init();
            ditGames.enemies.updatePos(ditGames.vars.infoUser.yPosP, ditGames.vars.infoUser.xPosP);
        });
        jQuery('body').on('touchend touchcancel', function(e) {
            ditGames.counter.clear();
        });
    }
};

/*** start enemies ***/
ditGames.enemies = {
    updatePos: function(yPos, xPos) {
        $('.enemy').css({
            top: yPos,
            left: xPos
        });
    }
};
/*** end enemies ***/
function touchHandler(event) {
    var touch = event.changedTouches[0];

    var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent({
            touchstart: "mousedown",
            touchmove: "mousemove",
            touchend: "mouseup"
        }[event.type], true, true, window, 1,
        touch.screenX, touch.screenY,
        touch.clientX, touch.clientY, false,
        false, false, false, 0, null);

    touch.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function init() {
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);
};
$(function() {
    init();                     // iniciamos eventos touch
    ditGames.dit.init();        // iniciamos evento draggable
    ditGames.dit.touches();     // iniciamos funciones para touchstart,touchend...
});