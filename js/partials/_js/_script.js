/**
 * @file dit.js
 *
 *
 */
ditGames.counter = {
    init: function() {
        ditGames.vars.level++;
        $('.level').text(ditGames.vars.level);
    },
    clear: function(){
        clearTimeout();
    }
};

ditGames.dit = {
    init: function() {
        $('#box').draggable({
            containment: "parent"
        });
    },
    touches: function(){
        jQuery('body').on('touchstart touchend touchmove', function(e) {
            var xPos = e.originalEvent.touches[0].pageX;
            var yPos = e.originalEvent.touches[0].pageY;
            ditGames.vars.infoUser.yPosP = yPos;
            ditGames.vars.infoUser.xPosP = xPos;
            $('.posicion').text('Left: ' + xPos + ' /  Top: ' + yPos);
            $('#box').css({
                top: (yPos - 50),
                left: (xPos - 50)
            });
        });
        jQuery('body').on('touchstart touchmove', function(e) {
            ditGames.counter.init();
            ditGames.enemies.updatePos(ditGames.vars.infoUser.yPosP, ditGames.vars.infoUser.xPosP);
        });
        jQuery('body').on('touchend touchcancel', function(e) {
            ditGames.counter.clear();
        });
    }
};

/*** start enemies ***/
ditGames.enemies = {
    updatePos: function(yPos, xPos) {
        $('.enemy').css({
            top: yPos,
            left: xPos
        });
    }
};
/*** end enemies ***/